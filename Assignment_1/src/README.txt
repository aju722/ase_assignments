************Requirements*****************
//Must have installed:
	Nodejs
	npm

//optional for continuously updating site
	nodemon

****************Run**********************
    //static site
    	node main.js

    //continuously updating site
    	npm run dev
	
    //all are listening on port 3000
	
	//the get request for properties as json
	localhost:3000/properties


    //To stop the server
    	ctrl+c


