

const express = require('express');

//Init express
const app = express();

const port = 3000

const path = require('path');

/*
	id, address, and zip order by id
*/

const prop = require('./properties');

const hi = require('./hello');

const logger = (req, res, next) =>
{
	console.log('Hello');
	next();
}

app.get('/hello', (req, res) =>
{
		res.json(hi)
});

app.get('/properties', (req, res) =>
{
	res.json(prop);
});

//set up a static folder
app.use(express.static(path.join(__dirname, 'public')));

/*

app.get('/', (req, res) =>
{
	res.sendFile(path.join(__dirname, 'public', 'data.html'));
});


app.get('/', (req, res) => res.send('Main hello World'));

*/
app.listen(port, () => console.log('Server listening on port 3000.'));


/*
create you endpoints/route handlers
app.get('/', function(req, res)
	{
	res.send('Hello World!');
	Fetch from database
	load pages
	Return json
	Full access to request & response
	});
*/
