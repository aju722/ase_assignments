/*
	id, address, and zip order by id
*/

const properties = 
[
 {
	id: 1,
	address: '131 example st',
	zip: '78954'
 },
 {
 	id: 2,
 	address: '213 king st',
 	zip: '95566'
 }
];

module.exports = properties;